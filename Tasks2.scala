package scala_tasks

import scala.annotation.tailrec

object Tasks2 extends App{

  //P31 - isPrime
  def isPrime(n: Int): Boolean = {
    @tailrec
    def primeUntilFunc(t: Int): Boolean = if (t <= 1) true else n % t != 0 && primeUntilFunc(t - 1)
    primeUntilFunc(n / 2)
  }
  println("P31: "+isPrime(20))

  //P32 - GCD
  def gcd(n : Int, k : Int): Int ={
    if(n < k) gcd(k,n)
    if(n%k==0) k
    else gcd(k,n%k)
  }
  println("P32: "+gcd(46332 ,71162))

  //P33 - isCoprimeTo
  def isCoprimeTo(n : Int, k : Int): Boolean = gcd(n,k) == 1

  println("P33: "+isCoprimeTo(35,64))

  //P35 - prime factors
  def primeFactors(k : Int) : List[Int]  = {
    @tailrec
    def help_primeFactors(n : Int, list : List[Int]): List[Int] = {
      if(n <= 1) list
      else if (isPrime(n) && k%n==0) help_primeFactors(n-1,n::list)
      else help_primeFactors(n-1,list)
    }
    help_primeFactors(k, List())
  }

  println("P35: "+primeFactors(315))

  //P39
  def listPrimeRanges(n : Int, k : Int): List[Int] ={
    List.range(n,k+1).filter( x => isPrime(x))
  }

  println("P39: "+listPrimeRanges(7,553))

  //P40
  def goldbach(n : Int) : (Int, Int) = {
    if(n < 3 || n % 2 == 1) throw new Exception("Your number less then 3 and/or uneven")
    val x = listPrimeRanges(2,n/4).find(x => isPrime(n - x)).get
    (x,n-x)
  }

  println("P40: "+goldbach(28))

  //P41
  def printGoldbachListLimited(start : Int, finish : Int, min : Int) : Unit = {
    List.range(start,finish).filter(n => n > 2 && n % 2 == 0).map(n=> (n,goldbach(n))).
      filter(n => n._2._1 > min).foreach( x => println(x._1 + " = " + x._2._1 + " + " + x._2._2))
  }

  println("P41: ")
  printGoldbachListLimited(11400,11700,100)
}

