package scala_tasks

import scala.util.Random

object Tasks1 extends App {

  val list = List(1, 3, 0, 2, 3, 4, 5, 6, 2, 8, 8)
  val nestedList = List(List(1, 3, 0, 2, 3, 4, 5, 6, 2, 8, 8), 23, 22, List(0))
  val symbolList = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
  val compressedSList : List[(Int,Symbol)] = List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
  val forSortList = List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o))

  //P01 - Найти последний элемент листа
  def last(list: List[Int]): Int = list.last

  println(last(list))

  //P02 - Найти последний элемент листа, встречающийся в листе один раз
  def penultimate(list: List[Int]): Int = list.filter(a => list.indexOf(a) == list.lastIndexOf(a)).last

  println(penultimate(list))

  //P03 - Найти k-ый элемент листа (пусть
  def nth(k: Int, list: List[Int]): Int = {
    if (k >= 0 && k < length(list) ) list(k)
    else throw new ArrayIndexOutOfBoundsException
  }

  println(nth(2, list))

  //P04 - Найти длину листа
  def length(list: List[Int]): Int = list.length

  println(length(list))

  //P05 - Развернуть лист
  def reverse(list: List[Int]): List[Int] = list.reverse

  println(reverse(list))

  //P06 - Является ли лист палиндромом
  def isPalindrome(list: List[Int]): Boolean = list == list.reverse

  println(isPalindrome(list))

  //P07 - Слить "Лист листов и значений" в один лист
  def flatten(nList: List[Any]): List[Any] = nList flatMap {
    case ms: List[ms] => flatten(ms)
    case x => List(x)
    }

  println(flatten(nestedList))

  //P08 - Убрать последовательные дубликаты
  def compress(sList: List[Symbol]): List[Symbol] = sList match {
    case Nil => Nil
    case h :: List() => List(h)
    case h :: tail if h == tail.head => compress(tail)
    case h :: tail => h :: compress(tail)
  }

  println(compress(symbolList))

  //P09
  def pack(list: List[Symbol]) : List[List[Symbol]] = {
    if(list.nonEmpty){
      val tuple1 = list.span(_ == list.head)
      if(tuple1._2 == Nil) List(tuple1._1)
      else tuple1._1::pack(tuple1._2)
    }
    else List(List())
  }
  println(pack(symbolList))

  //P10
  def encode(list: List[Symbol]) : List[(Int,Symbol)]  = pack(list).map(x => (x.length,x.head))
  println(encode(symbolList))

  //P11
  def encodeModified(list: List[Symbol]) : List[Any]  = encode(list).map(x => if(x._1 == 1) x._2 else x)
  println(encodeModified(symbolList))

  //P12
  def decode(list : List[Symbol]) : List[Symbol] = encode(list).flatMap(x => List.fill(x._1)(x._2))
  println(decode(symbolList))

  //P13
  def encodeDirect(list : List[Symbol]) : List[(Int,Symbol)] = {
    if (list.nonEmpty) {
      val t = list.span(_ == list.head)
      (t._1.length,t._1.head)::encodeDirect(t._2)
    }
    else Nil
  }
  println(encodeDirect(symbolList))

  //P14
  def duplicate(list: List[Symbol]): List[Symbol] = list.flatMap(x => List(x, x))

  println(duplicate(symbolList))

  //P15
  def duplicateN(n: Int, list: List[Symbol]): List[Symbol] = list.flatMap(x => List.fill(n)(x))

  println(duplicateN(3, symbolList))

  //P16
  def drop(n: Int, list: List[Symbol]): List[Symbol] = list.grouped(n).flatMap(x => x.take(n - 1)).toList

  println(drop(3, symbolList))

  //P17
  def split(n: Int, list: List[Symbol]): (List[Symbol], List[Symbol]) = list.splitAt(n)

  println(split(4, symbolList))

  //P18
  def slice(n: Int, k: Int, list: List[Symbol]): List[Symbol] = list.slice(n, k)

  println(slice(1, 5, symbolList))

  //P19
  def rotate(n: Int, list: List[Symbol]): List[Symbol] = {
    if (n > 0) {
      List.concat(list.slice(n, list.length), list.slice(0, n))
    }
    else {
      List.concat(list.slice(list.length + n, list.length), list.slice(0, list.length + n))
    }
  }

  println(rotate(-1, symbolList))

  //P20
    def removeAt[T](n: Int, list: List[T]): (List[T], T) = (list.take(n - 1) ::: list.takeRight(list.length - n), list(n))

  println(removeAt(6, symbolList))

  //P21
  def insertAt(s: Symbol, n: Int, list: List[Symbol]): List[Symbol] = list.take(n) ::: List(s) ::: list.takeRight(list.length - n)

  println(insertAt(Symbol("smth"), 2, symbolList))

  //P22
  def range(n: Int, k: Int): List[Int] = Range(n, k + 1).toList

  println(range(4, 9))

  //P23
  def randomSelect[T](n: Int, list: List[T]): List[T] = {
    if (n == 0) Nil
    else {
      val tuple1 = removeAt(new Random().nextInt(list.length), list)
      tuple1._2 :: randomSelect(n - 1, tuple1._1)
    }
  }

  println(randomSelect(3, symbolList))

  //P24
  def lotto(n: Int, k: Int): List[Int] = randomSelect(n,List.range(1,k+1))

  println(lotto(6, 49))

  //P25
  def randomPermute(list: List[Symbol]): List[Symbol] = randomSelect(list.length,list)

  println(randomPermute(symbolList))

  //P26
  //P27

  //P28 - a
  def lsort(list : List[List[Symbol]]) : List[List[Symbol]] = list.sortBy(x => x.length)
  println(lsort(forSortList))

  //P28 - b
  def lsortFreq(list : List[List[Symbol]]) : List[List[Symbol]] = {
    def freq(length : Int): Int = list.count(x => x.length == length)
    list.sortBy(x=> freq(x.length))
  }
  println(lsortFreq(forSortList))
}
